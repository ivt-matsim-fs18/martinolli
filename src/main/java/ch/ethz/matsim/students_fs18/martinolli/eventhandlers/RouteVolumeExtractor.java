package ch.ethz.matsim.students_fs18.martinolli.eventhandlers;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.events.LinkEnterEvent;
import org.matsim.api.core.v01.events.VehicleEntersTrafficEvent;
import org.matsim.api.core.v01.events.VehicleLeavesTrafficEvent;
import org.matsim.api.core.v01.events.handler.LinkEnterEventHandler;
import org.matsim.api.core.v01.events.handler.VehicleEntersTrafficEventHandler;
import org.matsim.api.core.v01.events.handler.VehicleLeavesTrafficEventHandler;
import org.matsim.api.core.v01.network.Link;

import java.util.List;

/**
 * @author thibautd
 */
public class RouteVolumeExtractor implements LinkEnterEventHandler,
		VehicleEntersTrafficEventHandler, VehicleLeavesTrafficEventHandler {
	private final RouteTracker routeTracker = new RouteTracker();
	private final RouteLabeler labeler = new RouteLabeler();
	private final VolumeStreamer<List<Id<Link>>> volumeWriter;
	private final String routeFilePath;

	private int resetCount = 0;

	public RouteVolumeExtractor( String volumeFilePath , String routeFilePath, double timeStep_s) {
		this.volumeWriter = new VolumeStreamer<>( volumeFilePath, timeStep_s, "route", labeler::label);
		this.routeFilePath = routeFilePath;
	}

	@Override
	public void reset(int iteration) {
		if (resetCount++ > 0) throw new IllegalStateException( getClass().getName()+" is not thought to be used in several iterations" );
	}

	public void close() {
		volumeWriter.close();
		labeler.writeLabels( routeFilePath );
	}

	@Override
	public void handleEvent( final LinkEnterEvent event ) {
		routeTracker.addEvent( event.getVehicleId(), event.getLinkId(), event.getTime() );
	}

	@Override
	public void handleEvent( final VehicleEntersTrafficEvent event ) {
		routeTracker.addEvent( event.getVehicleId(), event.getLinkId(), event.getTime() );
	}

	@Override
	public void handleEvent( final VehicleLeavesTrafficEvent event ) {
		RouteTracker.Route r = routeTracker.removeRoute( event.getVehicleId() );
		// put routes in time bin corresponding to the arrival time, as this ensures records are added by increasing
		// time and thus allows streaming. Should figure out another solution in case something else is needed.
		volumeWriter.addEvent( r.getLinks() , event.getTime() );
	}
}
