package ch.ethz.matsim.students_fs18.martinolli.eventhandlers;

import org.apache.log4j.Logger;
import org.matsim.core.utils.io.IOUtils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * @author thibautd
 */
public class VolumeStreamer<K> implements AutoCloseable {
	private static final Logger log = Logger.getLogger( VolumeStreamer.class );
	private final BufferedWriter writer;
	private final double timeStep_s;

	private final Function<K,String> keyPrinter;

	private double currentStartTime = 0;

	private final Map<K,Integer> counts = new HashMap<>();

	public VolumeStreamer(final String file,
			final double timeStep_s,
			final String header) {
		this(file, timeStep_s, header, Object::toString);
	}

	public VolumeStreamer(final String file,
			final double timeStep_s,
			final String header,
			final Function<K,String> keyPrinter) {
		this.keyPrinter = keyPrinter;
		this.timeStep_s = timeStep_s;
		this.writer = IOUtils.getBufferedWriter(file);
		try {
			writer.write( header+"\tstartTime\tendTime\tvolume" );
		}
		catch ( IOException e ) {
			throw new UncheckedIOException( e );
		}
	}

	public void addEvent(K key, double time) {
		if ( time < currentStartTime ) {
			throw new IllegalStateException( "volume streamer can only get events in increasing time," +
					" but "+time+" < "+currentStartTime );
		}
		while ( time > currentStartTime + timeStep_s ) {
			if (log.isTraceEnabled()) log.trace( time+" > "+(currentStartTime + timeStep_s) );
			dumpAndStep();
		}
		increaseCount(key);
	}

	private void increaseCount( final K key ) {
		counts.merge( key , 1 , ( a , b ) -> a + b );
	}

	private void dumpAndStep() {
		counts.forEach( (k,c) -> {
			try {
				writer.newLine();
				writer.write( keyPrinter.apply(k)+"\t"+currentStartTime+"\t"+(currentStartTime+timeStep_s)+"\t"+c );
			}
			catch ( IOException e ) {
				throw new UncheckedIOException( e );
			}
		} );
		counts.clear();
		if (log.isTraceEnabled()) log.trace("step from "+currentStartTime);
		currentStartTime += timeStep_s;
		if (log.isTraceEnabled()) log.trace("stepped to "+currentStartTime);
	}

	@Override
	public void close() {
		try {
			dumpAndStep();
			this.writer.close();
		}
		catch ( IOException e ) {
			throw new UncheckedIOException( e );
		}
	}
}
