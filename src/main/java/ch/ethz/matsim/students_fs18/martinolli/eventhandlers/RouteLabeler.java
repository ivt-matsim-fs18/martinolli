package ch.ethz.matsim.students_fs18.martinolli.eventhandlers;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.core.utils.io.IOUtils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author thibautd
 */
public class RouteLabeler {
	private final Map<List<Id<Link>>, String> labels = new HashMap<>();
	private int currentLabel = 0;

	public String label(List<Id<Link>> route) {
		return labels.computeIfAbsent( route , r -> ""+currentLabel++ );
	}

	public void writeLabels(String file) {
		try ( final BufferedWriter writer = IOUtils.getBufferedWriter( file ) ) {
			writer.write("routeId\tlinkId");
			labels.forEach( (r, rId) -> r.forEach( lId -> {
				try {
					writer.newLine();
					writer.write( rId+"\t"+lId );
				}
				catch ( IOException e ) {
					throw new UncheckedIOException( e );
				}
			} ) );
		}
		catch ( IOException e ) {
			throw new UncheckedIOException( e );
		}
	}
}
