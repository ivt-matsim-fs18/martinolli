package ch.ethz.matsim.students_fs18.martinolli.eventhandlers;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.vehicles.Vehicle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author thibautd
 */
public class RouteTracker {
	private final Map<Id<Vehicle>,Route> currentRoutes = new HashMap<>();

	public void addEvent(Id<Vehicle> vehicleId, Id<Link> linkId, double time) {
		currentRoutes.computeIfAbsent( vehicleId , k -> new Route(time) )
				.addLink( time , linkId );
	}

	public Route removeRoute(Id<Vehicle> vehicleId) {
		return currentRoutes.remove( vehicleId );
	}

	public static class Route {
		private final double start;
		private double end = -1;
		private List<Id<Link>> links = new ArrayList<>();

		private Route( final double start ) {
			this.start = start;
		}

		private void addLink(double time, Id<Link> link) {
			this.links.add(link);
			this.end = time;
		}

		public double getStart() {
			return start;
		}

		public double getEnd() {
			return end;
		}

		public List<Id<Link>> getLinks() {
			return links;
		}
	}
}
