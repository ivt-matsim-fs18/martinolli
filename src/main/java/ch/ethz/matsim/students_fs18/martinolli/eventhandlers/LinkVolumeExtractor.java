package ch.ethz.matsim.students_fs18.martinolli.eventhandlers;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.events.LinkEnterEvent;
import org.matsim.api.core.v01.events.VehicleEntersTrafficEvent;
import org.matsim.api.core.v01.events.handler.LinkEnterEventHandler;
import org.matsim.api.core.v01.events.handler.VehicleEntersTrafficEventHandler;
import org.matsim.api.core.v01.network.Link;

/**
 * @author thibautd
 */
public class LinkVolumeExtractor implements LinkEnterEventHandler, VehicleEntersTrafficEventHandler {
	private final VolumeStreamer<Id<Link>> volumeWriter;

	private int resetCount = 0;

	public LinkVolumeExtractor( String filePath , double timeStep_s) {
		this.volumeWriter = new VolumeStreamer<>( filePath, timeStep_s, "link" );
	}

	@Override
	public void reset(int iteration) {
		if (resetCount++ > 0) throw new IllegalStateException( getClass().getName()+" is not thought to be used in several iterations" );
	}

	public void close() {
		volumeWriter.close();
	}

	@Override
	public void handleEvent( final LinkEnterEvent event ) {
		volumeWriter.addEvent( event.getLinkId() , event.getTime() );
	}

	@Override
	public void handleEvent( final VehicleEntersTrafficEvent event ) {
		volumeWriter.addEvent( event.getLinkId() , event.getTime() );
	}
}
