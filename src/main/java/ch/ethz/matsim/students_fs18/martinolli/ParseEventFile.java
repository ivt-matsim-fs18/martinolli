package ch.ethz.matsim.students_fs18.martinolli;

import ch.ethz.matsim.students_fs18.martinolli.eventhandlers.LinkVolumeExtractor;
import ch.ethz.matsim.students_fs18.martinolli.eventhandlers.RouteVolumeExtractor;
import org.matsim.core.api.experimental.events.EventsManager;
import org.matsim.core.events.EventsUtils;
import org.matsim.core.events.MatsimEventsReader;

import java.io.File;

/**
 * usage: ParseEventFile path/to/eventsFile.xml output/directory/
 *
 * @author thibautd
 */
public class ParseEventFile {
	/**
	 * This is the time bin size, in seconds.
	 */
	private static final double TIME_STEP = 15 * 60;

	public static void main( final String... args ) {
		String eventsFile = args[0];
		String outputDirectory = args[1];

		if (new File(outputDirectory).exists()) {
			throw new RuntimeException( "Directory "+outputDirectory+" exists. Please delete it before continuing" );
		}
		new File(outputDirectory).mkdirs();

		final LinkVolumeExtractor linkHandler =
				new LinkVolumeExtractor(
						outputDirectory+"/linkVolumes.dat",
						TIME_STEP );
		final RouteVolumeExtractor routeHandler =
				new RouteVolumeExtractor(
						outputDirectory+"/routeVolumes.dat",
						outputDirectory+"/routeLinks.dat",
						TIME_STEP );

		final EventsManager eventsManager = EventsUtils.createEventsManager();
		eventsManager.addHandler( linkHandler );
		eventsManager.addHandler( routeHandler );
		new MatsimEventsReader( eventsManager ).readFile( eventsFile );
		linkHandler.close();
		routeHandler.close();
	}
}
